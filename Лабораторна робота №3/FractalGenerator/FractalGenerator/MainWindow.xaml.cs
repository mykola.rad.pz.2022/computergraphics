﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FractalGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        private double lastX, lastY;
        private Dictionary<string, Color[]> _colors = new Dictionary<string, Color[]>
        {
            {"Red", new[] {Color.FromRgb(139, 0, 0), Color.FromRgb(255, 0, 0), Color.FromRgb(255, 127, 0), Color.FromRgb(255, 69, 0), Color.FromRgb(255, 99, 71), Color.FromRgb(255, 192, 203), Color.FromRgb(255, 0, 255), Color.FromRgb(255, 105, 180)}},
            {"Green", new[] {Color.FromRgb(0, 100, 0), Color.FromRgb(0, 128, 0), Color.FromRgb(127, 255, 0), Color.FromRgb(0, 255, 0), Color.FromRgb(128, 128, 0), Color.FromRgb(47, 79, 79), Color.FromRgb(0, 255, 127), Color.FromRgb(0, 255, 255), Color.FromRgb(127, 255, 212)}} ,
            {"Blue", new[] {Color.FromRgb(0, 0, 128), Color.FromRgb(0, 0, 255), Color.FromRgb(0, 191, 255), Color.FromRgb(75, 0, 130), Color.FromRgb(0, 255, 255), Color.FromRgb(0, 128, 128), Color.FromRgb(138, 43, 226), Color.FromRgb(139, 0, 255), Color.FromRgb(221, 160, 221), Color.FromRgb(230, 230, 250)}},
            {"Black", new[] {Color.FromRgb(0, 0, 0), Color.FromRgb(128, 128, 128), Color.FromRgb(169, 169, 169), Color.FromRgb(211, 211, 211), Color.FromRgb(105, 105, 105), Color.FromRgb(51, 51, 51), Color.FromRgb(192, 192, 192), Color.FromRgb(70, 130, 180), Color.FromRgb(52, 73, 94)}},
            {"White", new[] {Color.FromRgb(255, 255, 255), Color.FromRgb(255, 255, 240), Color.FromRgb(255, 253, 208), Color.FromRgb(253, 245, 230), Color.FromRgb(250, 250, 250), Color.FromRgb(245, 245, 245), Color.FromRgb(230, 230, 250), Color.FromRgb(240, 248, 255), Color.FromRgb(160, 160, 160)}}
        };

        private const int INTERVAL = 20;


        public MainWindow()
        {
            InitializeComponent();
            InitializeColorComboBox();
        }

        private void InitializeColorComboBox()
        {
            colorCB.ItemsSource = _colors.Keys.ToList();
            colorCB.SelectedIndex = 0;
        }

        private int ColorToInt32(Color color)
        {
            return (int)((color.A << 24) | (color.R << 16) | (color.G << 8) | color.B);
        }

        private Color GetColor(Point point, double c, Color[] colors)
        {
            Complex z = new Complex(point.X, point.Y);
            Complex constant = new Complex(c, 0);
            int iterations = (int)IterationCountSlider.Value;
            
            int i = 0;
            do
            {
                z = Complex.Sinh(z);
                z = Complex.Add(z,constant);
                ++i;
            } while (i < iterations && ((z.Real * z.Real + z.Imaginary * z.Imaginary) < 100));

            return i == iterations ? Colors.Black : colors[i % colors.Length];
        }

        /*private void openFile_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Title = "Open Image",
                Filter = "PNG Image|*.png"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var image = new BitmapImage(new Uri(openFileDialog.FileName));
                    ((Image)canvas.Children[0]).Source = image;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error opening image: " + ex.Message);
                }
            }
        }

        private void saveFile_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Save Image",
                Filter = "PNG Image|*.png",
                FileName = "fractal.png"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)((Image)canvas.Children[0]).Source));
                using (var stream = saveFileDialog.OpenFile())
                {
                    encoder.Save(stream);
                }
            }
        }*/

        private void openFile_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Title = "Open Image",
                Filter = "PNG Image|*.png"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var image = new BitmapImage(new Uri(openFileDialog.FileName));
                    var newImage = new Image();
                    newImage.Source = image;
                    canvas.Children.Clear();
                    canvas.Children.Add(newImage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error opening image: " + ex.Message);
                }
            }
        }

        private void saveFile_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Save Image",
                Filter = "PNG Image|*.png",
                FileName = "fractal.png"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    var renderBitmap = new RenderTargetBitmap((int)canvas.ActualWidth, (int)canvas.ActualHeight, 96, 96, PixelFormats.Pbgra32);
                    renderBitmap.Render(canvas);

                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(renderBitmap));

                    using (var stream = saveFileDialog.OpenFile())
                    {
                        encoder.Save(stream);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error saving image: " + ex.Message);
                }
            }
        }

        private double GetCoordinateFromText(string text)
        {
            return double.TryParse(text, out var result) ? result : 0;
        }

        private void сomboBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            CollapseStackPanels();
            ShowHiddenStackPanels();
            HideVisibleStackPanels();
        }

        private void ShowHiddenStackPanels()
        {
            foreach (var stackPanel in FindVisualChildren<StackPanel>(this))
            {
                if (stackPanel.Style != null && stackPanel.Style == (Style)FindResource("Hidden"))
                {
                    stackPanel.Style = (Style)FindResource("Visible");
                }
            }
        }

        private void CollapseStackPanels()
        {
            foreach (var stackPanel in FindVisualChildren<StackPanel>(this))
            {
                if (stackPanel.Style != null && stackPanel.Style == (Style)FindResource("Visible"))
                {
                    stackPanel.Style = (Style)FindResource("Collapsed");
                }
            }
        }

        private void HideVisibleStackPanels()
        {
            foreach (var stackPanel in FindVisualChildren<StackPanel>(this))
            {
                if (stackPanel.Style != null && stackPanel.Style == (Style)FindResource("Collapsed"))
                {
                    stackPanel.Style = (Style)FindResource("Hidden");
                }
            }
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject dependencyObject) where T : DependencyObject
        {
            if (dependencyObject != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(dependencyObject); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(dependencyObject, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        private bool validateDataGeometric(out int depth, out double length, out double centerX, out double centerY)
        {
            if (!int.TryParse(txtDepth.Text, out depth))
            {
                MessageBox.Show("Неправильне значення глибини. Будь ласка, введіть ціле число.", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                centerX = centerY = length = double.NaN;
                return false;
            }

            if (!double.TryParse(x.value.Text, out centerX) || !double.TryParse(y.value.Text, out centerY))
            {
                MessageBox.Show("Неправильне значення центру. Будь ласка, введіть дійсні числа.", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                centerY = length = double.NaN;
                return false;
            }

            if (!double.TryParse(txtLength.Text, out length))
            {
                MessageBox.Show("Неправильне значення довжини. Будь ласка, введіть дійсне число.", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }

        private void CreateAxises()
        {
            CreateGraphAxis();

            // Створення підписів
            TextBlock text = new TextBlock();
            text.Text = "0";
            text.Margin = new Thickness((canvas.Width / 2) + 2, canvas.Height / 2 + 2, 0, 0);
            canvas.Children.Add(text);

            TextBlock textX = new TextBlock();
            textX.Text = "X";
            textX.Margin = new Thickness(canvas.Width - 10, canvas.Height / 2 + INTERVAL, 0, 0);
            canvas.Children.Add(textX);

            TextBlock textY = new TextBlock();
            textY.Text = "Y";
            textY.Margin = new Thickness((canvas.Width / 2) + INTERVAL, 0, 0, 0);
            canvas.Children.Add(textY);

            for (int i = INTERVAL, j = (-1) * i; i < (int)(canvas.Width / 2) && j > (int)(canvas.Width / 2 * (-1)); i += INTERVAL, j -= INTERVAL)
            {
                WriteHorAxis(i, 5);
                WriteHorAxis(j, -15);
            }

            for (int i = INTERVAL, j = (-1) * i; i < (int)(canvas.Height / 2) && j > (int)(canvas.Height / 2 * (-1)); i += INTERVAL, j -= INTERVAL)
            {
                WriteVerAxis(i, 0);
                WriteVerAxis(j, 0);
            }
        }
        private void CreateGraphAxis()
        {
            Line horAxis = new Line();
            Line verAxis = new Line();

            horAxis.X1 = 0;
            horAxis.X2 = canvas.Width;
            horAxis.Y1 = horAxis.Y2 = canvas.Height / 2;
            horAxis.Stroke = Brushes.Black;
            horAxis.StrokeThickness = 2;

            Line x_RightArrow = new Line();
            x_RightArrow.X1 = canvas.Width;
            x_RightArrow.X2 = canvas.Width - 10;
            x_RightArrow.Y1 = canvas.Height / 2;
            x_RightArrow.Y2 = canvas.Height / 2 - 10;
            x_RightArrow.Stroke = Brushes.Black;
            x_RightArrow.StrokeThickness = 2;
            canvas.Children.Add(x_RightArrow);

            Line x_LeftArrow = new Line();
            x_LeftArrow.X1 = canvas.Width;
            x_LeftArrow.X2 = canvas.Width - 10;
            x_LeftArrow.Y1 = canvas.Height / 2;
            x_LeftArrow.Y2 = canvas.Height / 2 + 10;
            x_LeftArrow.Stroke = Brushes.Black;
            x_LeftArrow.StrokeThickness = 2;
            canvas.Children.Add(x_LeftArrow);


            verAxis.X1 = verAxis.X2 = canvas.Width / 2;
            verAxis.Y1 = 0;
            verAxis.Y2 = canvas.Height;
            verAxis.Stroke = Brushes.Black;
            verAxis.StrokeThickness = 2;

            Line y_RightArrow = new Line();
            y_RightArrow.X1 = canvas.Width / 2;
            y_RightArrow.X2 = canvas.Width / 2 + 10;
            y_RightArrow.Y1 = 0;
            y_RightArrow.Y2 = 10;
            y_RightArrow.Stroke = Brushes.Black;
            y_RightArrow.StrokeThickness = 2;
            canvas.Children.Add(y_RightArrow);

            Line y_LeftArrow = new Line();
            y_LeftArrow.X1 = canvas.Width / 2;
            y_LeftArrow.X2 = canvas.Width / 2 - 10;
            y_LeftArrow.Y1 = 0;
            y_LeftArrow.Y2 = 10;
            y_LeftArrow.Stroke = Brushes.Black;
            y_LeftArrow.StrokeThickness = 2;
            canvas.Children.Add(y_LeftArrow);


            canvas.Children.Add(horAxis);
            canvas.Children.Add(verAxis);
        }

        private void WriteHorAxis(int position, int offset)
        {
            // Створення підписів, позначок та сітки (горизонтально)
            TextBlock textX = new TextBlock();
            textX.Text = (position / INTERVAL).ToString();
            textX.Margin = new Thickness((canvas.Width / 2) + position + offset, canvas.Height / 2 + 2, 0, 0);
            canvas.Children.Add(textX);

            Line markX = new Line();
            markX.X1 = (canvas.Width / 2) + position;
            markX.X2 = (canvas.Width / 2) + position;
            markX.Y1 = 0;
            markX.Y2 = canvas.Height;
            markX.Stroke = Brushes.Black;
            markX.StrokeThickness = 0.5;
            canvas.Children.Add(markX);

            Line _markX = new Line();
            _markX.X1 = (canvas.Width / 2) + position;
            _markX.X2 = (canvas.Width / 2) + position;
            _markX.Y1 = (canvas.Height / 2) - 5;
            _markX.Y2 = (canvas.Height / 2) + 5;
            _markX.Stroke = Brushes.Black;
            _markX.StrokeThickness = 1.5;
            canvas.Children.Add(_markX);
        }

        private void WriteVerAxis(int position, int offset)
        {
            // Створення підписів, позначок та сітки (вертикально)
            TextBlock textY = new TextBlock();
            textY.Text = (position / INTERVAL).ToString();
            textY.Margin = new Thickness((canvas.Width / 2) + 5, canvas.Height / 2 - position + offset, 0, 0);
            canvas.Children.Add(textY);

            Line markY = new Line();
            markY.X1 = 0;
            markY.X2 = canvas.Width;
            markY.Y1 = canvas.Height / 2 + position;
            markY.Y2 = canvas.Height / 2 + position;
            markY.Stroke = Brushes.Black;
            markY.StrokeThickness = 0.5;
            canvas.Children.Add(markY);

            Line _markY = new Line();
            _markY.X1 = (canvas.Width / 2) + 5;
            _markY.X2 = (canvas.Width / 2) - 5;
            _markY.Y1 = canvas.Height / 2 + position;
            _markY.Y2 = canvas.Height / 2 + position;
            _markY.Stroke = Brushes.Black;
            _markY.StrokeThickness = 1.5;
            canvas.Children.Add(_markY);
        }

        private void DrawAlgebraicFractal()
        {
            var scaleX = 1 / ScaleSlider.Value;
            var offsetX = canvas.Width / 2;
            var offsetY = canvas.Height / 2;

            var bitmap = new WriteableBitmap((int)canvas.Width, (int)canvas.Height, 96, 96, PixelFormats.Bgr32, null);
            bitmap.Lock();


            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var zx = (x - offsetX) * scaleX;
                    var zy = (y - offsetY) * scaleX;
                    var color = GetColor(new Point(zx, zy), GetCoordinateFromText(c.value.Text), _colors[colorCB.SelectedItem.ToString()]);
                    bitmap.SetPixel(x, y, ColorToInt32(color));
                }
            }


            bitmap.Unlock();
            canvas.Children.Clear();
            canvas.Children.Add(new Image { Source = bitmap });
        }

        private void btnDraw_Click(object sender, RoutedEventArgs e)
        {
            canvas.Children.Clear();

            if (fractalCB.SelectedIndex == 0)
            {
                DrawGeometricFractal();
            }
            else
            {
                DrawAlgebraicFractal();
            }
        }

        /* private void DrawGeometricFractal()
 {
     if (!validateDataGeometric(out int depth, out double x, out double y))
     {
         return;
     }

     if (depth > 8)
     {
         if (MessageBox.Show("Велика глибина може зайняти багато часу на малювання (і все одно буде переважно чорною). Бажаєте продовжити?",
             "Продовжити?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
         {
             return;
         }
     }

     Image imgHilbert = new Image();

     imgHilbert.Width = canvas.ActualWidth;
     imgHilbert.Height = canvas.ActualHeight;

     Cursor = Cursors.Wait;
     UpdateLayout();

     double totalLength = Math.Min(imgHilbert.Width, imgHilbert.Height) * 0.9;
     double startX = (canvas.Width - totalLength) / 2;
     double startY = (canvas.Height - totalLength) / 2;
     double startLength = totalLength / (Math.Pow(2, depth) - 1);

     hilbertImage = new WriteableBitmap((int)imgHilbert.Width, (int)imgHilbert.Height, 96, 96, PixelFormats.Bgra32, null);
     imgHilbert.Source = hilbertImage;

     using (var context = hilbertImage.GetBitmapContext())
     {
         context.Clear();
         lastX = startX;
         lastY = startY;
         Hilbert(imgHilbert, depth, startLength, 0);
     }

     imgHilbert.InvalidateVisual();

     Cursor = Cursors.Arrow;
     canvas.Children.Add(imgHilbert);
 }*/

        /*  private void Hilbert(Image imgHilbert, int depth, double dx, double dy)
          {
              if (depth > 1) Hilbert(imgHilbert, depth - 1, dy, dx);
              DrawRelative(dx, dy);
              if (depth > 1) Hilbert(imgHilbert, depth - 1, dx, dy);
              DrawRelative(dy, dx);
              if (depth > 1) Hilbert(imgHilbert, depth - 1, dx, dy);
              DrawRelative(-dx, -dy);
              if (depth > 1) Hilbert(imgHilbert, depth - 1, -dy, -dx);

              imgHilbert.InvalidateVisual();
          }*/

        //private void DrawRelative(double dx, double dy)
        //{
        //    int x0 = (int)lastX;
        //    int y0 = (int)lastY;
        //    int x1 = (int)(lastX + dx);
        //    int y1 = (int)(lastY + dy);

        //    WriteableBitmapExtensions.DrawLine(hilbertImage, x0, y0, x1, y1, Colors.Black);

        //    lastX += dx;
        //    lastY += dy;
        //}

        private void DrawGeometricFractal()
        {
            CreateAxises();

            if (!validateDataGeometric(out int depth, out double length, out double x, out double y))
            {
                return;
            }

            if (depth > 8)
            {
                if (MessageBox.Show("Велика глибина може зайняти багато часу на малювання (і все одно буде переважно чорною). Бажаєте продовжити?",
                    "Продовжити?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
            }

            Cursor = Cursors.Wait;
            UpdateLayout();

            double totalLength = length * 20;
            double startX = (canvas.Width / 2 + x * INTERVAL) - totalLength / 2;
            double startY = (canvas.Height / 2 - y * INTERVAL) - totalLength / 2;

            if ((startX + totalLength / 2) > canvas.Width || startX < 0 
                || startY < 0 || (startY + totalLength / 2) > canvas.Height) 
            {
                MessageBox.Show("Надто велика довжина сторони. Будь ласка, зменшіть введене значення " +
                    "або оберіть інші координати для центру.", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                Cursor = Cursors.Arrow;
                return;
            }
            double startLength = totalLength / (Math.Pow(2, depth) - 1);

            lastX = startX;
            lastY = startY;

            Hilbert(depth, startLength, 0);

            Cursor = Cursors.Arrow;
        }

        private void Hilbert(int depth, double dx, double dy)
        {
            if (depth > 1) Hilbert(depth - 1, dy, dx);
            DrawRelative(dx, dy);
            if (depth > 1) Hilbert(depth - 1, dx, dy);
            DrawRelative(dy, dx);
            if (depth > 1) Hilbert(depth - 1, dx, dy);
            DrawRelative(-dx, -dy);
            if (depth > 1) Hilbert(depth - 1, -dy, -dx);
        }

        private void DrawRelative(double dx, double dy)
        {
            Line line = new Line();
            line.Stroke = Brushes.Black;
            line.StrokeThickness = 1;

            line.X1 = lastX;
            line.Y1 = lastY;
            line.X2 = lastX + dx;
            line.Y2 = lastY + dy;

            canvas.Children.Add(line);

            lastX += dx;
            lastY += dy;
        }
    }
}


// передбачити вихід за межі
// алгебраїчний фрактал