﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// Клас, що представляє користувацький елемент управління "TextBox" з можливістю сповіщення про зміну властивостей.
namespace BezieCreator.View.UserControls
{
    /// <summary>
    /// Interaction logic for TextBox.xaml
    /// </summary>
    public partial class TextBox : UserControl, INotifyPropertyChanged
    {
        private string myText; // Закрите поле тексту.

        public event PropertyChangedEventHandler? PropertyChanged; // Подія, яка виникає при зміні властивостей.

        public string MyText
        {
            get { return myText; }
            set
            {
                myText = value;
                OnPropertyChanged(); // Виклик події зміни властивостей.
            }
        }

        public TextBox()
        {
            DataContext = this; // Встановлення контексту даних для елементу управління.
            InitializeComponent();
        }

        private void tboxCoordinate_TextChanged(object sender, EventArgs e)
       {
            if (string.IsNullOrEmpty(value.Text))
                tbCoordinate.Visibility = Visibility.Visible;
            else
                tbCoordinate.Visibility = Visibility.Hidden;
        }

        // Метод, який сповіщає про зміну властивостей.
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
