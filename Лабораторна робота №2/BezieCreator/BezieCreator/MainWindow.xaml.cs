﻿using BezieCreator.View.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace BezieCreator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Polyline bezierCurvePolyline = new Polyline();
        private int _n = 1000; // Кількість точок для наближення кривої Безьє
        private bool created = false; // Прапорець для визначення, чи були створені осі графіка.
        private double _maxX; // Максимальне значення по осі X.
        private double _maxY; // Максимальне значення по осі Y.
        private const int INTERVAL = 20;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void bCreate_Clicked(object sender, EventArgs e)
        {
            bool[] check = new bool[2];

            check[0] = double.TryParse(maxX.value.Text, out _maxX);
            check[1] = double.TryParse(maxY.value.Text, out _maxY);

            // Перевірка на коректність введених даних
            if (Array.Exists(check, e => e == false) || _maxX <= 0 || _maxY <= 0 || _maxX > 100 || _maxY > 100)
            {
                MessageBox.Show("You have entered incorrect data!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Обчислення та встановлення висоти та ширини полотна відносно заданих користувачем максимальних значень х та у
            canvas.Width = (_maxX + 1) * 2 * INTERVAL;
            canvas.Height = (_maxY + 1) * 2 * INTERVAL;
            canvas.Children.Clear();

            // Створення осей
            CreateGraphAxis();

            // Створення підписів
            TextBlock text = new TextBlock();
            text.Text = "0";
            text.Margin = new Thickness((canvas.Width / 2) + 2, canvas.Height / 2 + 2, 0, 0);
            canvas.Children.Add(text);

            TextBlock textX = new TextBlock();
            textX.Text = "X";
            textX.Margin = new Thickness(canvas.Width - 10, canvas.Height / 2 + INTERVAL, 0, 0);
            canvas.Children.Add(textX);

            TextBlock textY = new TextBlock();
            textY.Text = "Y";
            textY.Margin = new Thickness((canvas.Width / 2) + INTERVAL, 0, 0, 0);
            canvas.Children.Add(textY);

            for (int i = INTERVAL, j = (-1) * i; i < (int)(canvas.Width / 2) && j > (int)(canvas.Width / 2 * (-1)); i += INTERVAL, j -= INTERVAL)
            {
                WriteHorAxis(i, 5);
                WriteHorAxis(j, -15);
            }

            for (int i = INTERVAL, j = (-1) * i; i < (int)(canvas.Height / 2) && j > (int)(canvas.Height / 2 * (-1)); i += INTERVAL, j -= INTERVAL)
            {
                WriteVerAxis(i, 0);
                WriteVerAxis(j, 0);
            }

            created = true;
        }

        private void bGenerate_Clicked(object sender, EventArgs e)
        {
            Clear();
            //n = cmbPoints.Items.Count;
            // Перевірка чи створено осі
            if (created is false)
            {
                MessageBox.Show("You have not created axises!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else if (cmbPoints.Items.Count < 2)
            {
                MessageBox.Show("You must add at least two points!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            List<Point> points = new List<Point>();

            // Iterate through all items in the combobox
            foreach (var item in cmbPoints.Items)
            {
                string pointString = item.ToString();

                // Remove square brackets and split into X and Y parts
                string[] parts = pointString.Trim('[', ']').Split(';');
                double.TryParse(parts[0], out double x);
                double.TryParse(parts[1], out double y);

                x = canvas.Width / 2 + x * INTERVAL;
                y = canvas.Height / 2 - y * INTERVAL;
                points.Add(new Point(x, y));
            }

            bool check = double.TryParse(tbStep.value.Text, out double tStep);

            if (!check || tStep <= 0)
            {
                MessageBox.Show("You have entered incorrect data for t-step!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!double.TryParse(tbStepX.value.Text, out double xStep) || xStep <= 0)
            {
                MessageBox.Show("You have entered incorrect data for x-step!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DrawCharacteristicFracture(points);
            //DrawBezierCurveRecursive(points, tStep);
            

            if (cmbMethod.SelectedItem == null)
                cmbMethod.SelectedItem = cmbMethod.Items[1];

            string selectedMethod = cmbMethod.SelectedItem.ToString();

            // Виклик відповідної функції в залежності від обраного методу
            switch (selectedMethod)
            {
                case "Recursive":
                    DrawBezierCurveRecursive(points, tStep);
                    break;
                default:
                    DrawBezierCurve(points, tStep);
                    break;
            }

            tbOutput.Text = CalculateBernsteinPolynomials(tStep, points);
            tbOutput.Text += PrintBezierPoints(xStep); // Виводимо точки кривої Безьє з кроком по осі X
        }

        private void Clear()
        {
            var polylines = canvas.Children.OfType<Polyline>().ToList();
            var ellipses = canvas.Children.OfType<Ellipse>().ToList();
            foreach (var polyline in polylines)
            {
                canvas.Children.Remove(polyline);
            }

            foreach (var ellipse in ellipses)
            {
                canvas.Children.Remove(ellipse);
            }

            canvas.InvalidateVisual();
            bezierCurvePolyline.Points.Clear();
        }

        private void bAdd_Clicked(object sender, RoutedEventArgs e)
        {
            if (!created)
            {
                MessageBox.Show("Create axises first!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            // Parse X and Y values from textboxes
            if (double.TryParse(X.value.Text, out double x) && double.TryParse(Y.value.Text, out double y)
                && x <= _maxX && x >= _maxX * (-1) && y <= _maxY && y >= _maxY * (-1))
            {
                // Create a new string with the format [x;y]
                string point = $"[{x} ; {y}]";

                if (!cmbPoints.Items.Contains(point))
                    cmbPoints.Items.Add(point);
                else
                    MessageBox.Show("You already have such a point!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
                MessageBox.Show("Invalid X or Y value!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void bDelete_Clicked(object sender, RoutedEventArgs e)
        {
            int selectedIndex = cmbPoints.SelectedIndex;
            cmbPoints.Items.RemoveAt(selectedIndex);
            bAdd.Visibility = Visibility.Visible;
            bSave.Visibility = Visibility.Collapsed;
            bDelete.Visibility = Visibility.Collapsed;
        }

        private void cmbPoints_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbPoints.SelectedItem != null)
            {
                bAdd.Visibility = Visibility.Collapsed;
                bSave.Visibility = Visibility.Visible;
                bDelete.Visibility = Visibility.Visible;

                string selectedPoint = cmbPoints.SelectedItem.ToString();
                string[] parts = selectedPoint.Trim('[', ']').Split(';');
                if (parts.Length == 2)
                {
                    X.value.Text = parts[0].Trim();
                    Y.value.Text = parts[1].Trim();
                }
            }
            else
            {
                bAdd.Visibility = Visibility.Visible;
                bSave.Visibility = Visibility.Collapsed;
                bDelete.Visibility = Visibility.Collapsed;
            }
        }

        private void bSave_Clicked(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(X.value.Text, out double x) && double.TryParse(Y.value.Text, out double y)
                && x <= _maxX && x >= _maxX * (-1) && y <= _maxY && y >= _maxY * (-1))
            {
                string point = $"[{x} ; {y}]";

                bool pointExists = false;
                foreach (var item in cmbPoints.Items)
                {
                    if (item.ToString() == point && item.ToString() != cmbPoints.SelectedItem.ToString())
                    {
                        pointExists = true;
                        break;
                    }
                }

                // Якщо такого значення немає, змінюємо вибране значення
                if (!pointExists && cmbPoints.SelectedItem != null)
                {
                    int selectedIndex = cmbPoints.SelectedIndex;
                    cmbPoints.Items[selectedIndex] = point;

                    bAdd.Visibility = Visibility.Visible;
                    bSave.Visibility = Visibility.Collapsed;
                    bDelete.Visibility = Visibility.Collapsed;
                }
                else
                    MessageBox.Show("You already have such a point!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
                MessageBox.Show("Invalid X or Y value!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void CreateGraphAxis()
        {
            Line horAxis = new Line();
            Line verAxis = new Line();

            horAxis.X1 = 0;
            horAxis.X2 = canvas.Width;
            horAxis.Y1 = horAxis.Y2 = canvas.Height / 2;
            horAxis.Stroke = Brushes.Black;
            horAxis.StrokeThickness = 2;

            Line x_RightArrow = new Line();
            x_RightArrow.X1 = canvas.Width;
            x_RightArrow.X2 = canvas.Width - 10;
            x_RightArrow.Y1 = canvas.Height / 2;
            x_RightArrow.Y2 = canvas.Height / 2 - 10;
            x_RightArrow.Stroke = Brushes.Black;
            x_RightArrow.StrokeThickness = 2;
            canvas.Children.Add(x_RightArrow);

            Line x_LeftArrow = new Line();
            x_LeftArrow.X1 = canvas.Width;
            x_LeftArrow.X2 = canvas.Width - 10;
            x_LeftArrow.Y1 = canvas.Height / 2;
            x_LeftArrow.Y2 = canvas.Height / 2 + 10;
            x_LeftArrow.Stroke = Brushes.Black;
            x_LeftArrow.StrokeThickness = 2;
            canvas.Children.Add(x_LeftArrow);


            verAxis.X1 = verAxis.X2 = canvas.Width / 2;
            verAxis.Y1 = 0;
            verAxis.Y2 = canvas.Height;
            verAxis.Stroke = Brushes.Black;
            verAxis.StrokeThickness = 2;

            Line y_RightArrow = new Line();
            y_RightArrow.X1 = canvas.Width / 2;
            y_RightArrow.X2 = canvas.Width / 2 + 10;
            y_RightArrow.Y1 = 0;
            y_RightArrow.Y2 = 10;
            y_RightArrow.Stroke = Brushes.Black;
            y_RightArrow.StrokeThickness = 2;
            canvas.Children.Add(y_RightArrow);

            Line y_LeftArrow = new Line();
            y_LeftArrow.X1 = canvas.Width / 2;
            y_LeftArrow.X2 = canvas.Width / 2 - 10;
            y_LeftArrow.Y1 = 0;
            y_LeftArrow.Y2 = 10;
            y_LeftArrow.Stroke = Brushes.Black;
            y_LeftArrow.StrokeThickness = 2;
            canvas.Children.Add(y_LeftArrow);


            canvas.Children.Add(horAxis);
            canvas.Children.Add(verAxis);
        }

        private void WriteHorAxis(int position, int offset)
        {
            // Створення підписів, позначок та сітки (горизонтально)
            TextBlock textX = new TextBlock();
            textX.Text = (position / INTERVAL).ToString();
            textX.Margin = new Thickness((canvas.Width / 2) + position + offset, canvas.Height / 2 + 2, 0, 0);
            canvas.Children.Add(textX);

            Line markX = new Line();
            markX.X1 = (canvas.Width / 2) + position;
            markX.X2 = (canvas.Width / 2) + position;
            markX.Y1 = 0;
            markX.Y2 = canvas.Height;
            markX.Stroke = Brushes.Black;
            markX.StrokeThickness = 0.5;
            canvas.Children.Add(markX);

            Line _markX = new Line();
            _markX.X1 = (canvas.Width / 2) + position;
            _markX.X2 = (canvas.Width / 2) + position;
            _markX.Y1 = (canvas.Height / 2) - 5;
            _markX.Y2 = (canvas.Height / 2) + 5;
            _markX.Stroke = Brushes.Black;
            _markX.StrokeThickness = 1.5;
            canvas.Children.Add(_markX);
        }

        private void WriteVerAxis(int position, int offset)
        {
            // Створення підписів, позначок та сітки (вертикально)
            TextBlock textY = new TextBlock();
            textY.Text = (position / INTERVAL).ToString();
            textY.Margin = new Thickness((canvas.Width / 2) + 5, canvas.Height / 2 - position + offset, 0, 0);
            canvas.Children.Add(textY);

            Line markY = new Line();
            markY.X1 = 0;
            markY.X2 = canvas.Width;
            markY.Y1 = canvas.Height / 2 + position;
            markY.Y2 = canvas.Height / 2 + position;
            markY.Stroke = Brushes.Black;
            markY.StrokeThickness = 0.5;
            canvas.Children.Add(markY);

            Line _markY = new Line();
            _markY.X1 = (canvas.Width / 2) + 5;
            _markY.X2 = (canvas.Width / 2) - 5;
            _markY.Y1 = canvas.Height / 2 + position;
            _markY.Y2 = canvas.Height / 2 + position;
            _markY.Stroke = Brushes.Black;
            _markY.StrokeThickness = 1.5;
            canvas.Children.Add(_markY);
        }

        private void DrawCirclePoint(List<Point> points, Color color)
        {
            foreach (var point in points)
            {
                Ellipse ellipse = new Ellipse();
                ellipse.Width = ellipse.Height = 10;
                ellipse.Fill = new SolidColorBrush(color);
                ellipse.Margin = new Thickness(point.X - 5, point.Y - 5, 0, 0);
                canvas.Children.Add(ellipse);
            }
        }

        private void DrawCharacteristicFracture(List<Point> points)
        {
            Polyline controlPolyline = new Polyline();
            // Малюємо керуючі точки
            controlPolyline.Stroke = Brushes.Green;
            controlPolyline.StrokeThickness = 1.5;
            controlPolyline.StrokeDashArray = new DoubleCollection() { 5 };
            foreach (Point point in points)
            {
                controlPolyline.Points.Add(point);
            }

            canvas.Children.Add(controlPolyline);

            List<Point> supportPoints = new List<Point>
            {
                points[0],
                points[^1]
            };

            List<Point> controlPoints = new List<Point>(points.ToArray()[1..^1]);

            DrawCirclePoint(supportPoints, Colors.Orange);
            DrawCirclePoint(controlPoints, Colors.Yellow);

        }

        private void DrawBezierCurveRecursive(List<Point> points, double step)
        {
            // Малюємо криву Безьє
            bezierCurvePolyline.Stroke = Brushes.Blue;
            bezierCurvePolyline.StrokeThickness = 2;

            Point[] bezierPoints = CalculateBezierPoints(points, step);
            foreach (Point point in bezierPoints)
            {
                bezierCurvePolyline.Points.Add(point);
            }
            canvas.Children.Add(bezierCurvePolyline);
        }

        private Point[] CalculateBezierPoints(List<Point> points, double step)
        {
            List<Point> bezierPoints = new List<Point>();
            for (double t = 0; t <= 1; t += step)
            {
                Point point = BezierInterpolation(points, t);
                bezierPoints.Add(point);
            }
            return bezierPoints.ToArray();
        }

        private Point BezierInterpolation(List<Point> points, double t)
        {
            // Рекурсивне обчислення точки на криві Безьє
            if (points.Count == 1)
                return points[0];

            List<Point> newPoints = new List<Point>();
            for (int i = 0; i < points.Count - 1; ++i)
            {
                double x = (1 - t) * points[i].X + t * points[i + 1].X;
                double y = (1 - t) * points[i].Y + t * points[i + 1].Y;
                newPoints.Add(new Point(x, y));
            }
            return BezierInterpolation(newPoints, t);
        }

        private void DrawBezierCurve(List<Point> points, double step)
        {
            // Малюємо криву Безьє
            bezierCurvePolyline.Stroke = Brushes.Blue;
            bezierCurvePolyline.StrokeThickness = 2;

            for (double t = 0; t <= 1; t += step) // Параметр t змінюється від 0 до 1 з певним кроком
            {
                Point point = CalculateBezierPoint(points, t);
                bezierCurvePolyline.Points.Add(point);
            }

            canvas.Children.Add(bezierCurvePolyline);
        }

        private Point CalculateBezierPoint(List<Point> points, double t)
        {
            int n = points.Count - 1;
            double x = 0, y = 0;

            for (int i = 0; i <= n; i++)
            {
                double bernstein = FindBernsteinPolynomial(n, i, t);
                x += points[i].X * bernstein;
                y += points[i].Y * bernstein;
            }

            return new Point(x, y);
        }

        private double FindBernsteinPolynomial(int n, int i, double t)
        {
            // Обчислення поліномів Бернштейна
            return BinomialCoefficient(n, i) * Math.Pow(t, i) * Math.Pow(1 - t, n - i);
        }

        private int BinomialCoefficient(int n, int i)
        {
            return FindFactorial(n) / (FindFactorial(i) * FindFactorial(n - i));
        }

        private int FindFactorial(int n)
        {
            if (n < 0)
                return 0;
            if (n == 0)
                return 1;

            int result = 1;
            for (int i = 1; i <= n; i++)
            {
                result *= i;
            }
            return result;
        }

        private string CalculateBernsteinPolynomials(double step, List<Point> controlPoints)
        {
            StringBuilder table = new StringBuilder();
            StringBuilder polynomials = new StringBuilder();
            polynomials.Append("|   t   |");
            for (int i = 0; i <= controlPoints.Count - 3; i++)
                polynomials.Append($"   b{controlPoints.Count - 1},{i}   |");

            table.AppendLine(polynomials.ToString());
            // Обчислюємо значення перших n-2 поліномів Бернштейна з заданим кроком
            for (double t = 0; t <= 1; t += step)
            {
                StringBuilder row = new StringBuilder();
                row.Append($"| {t.ToString("F4", CultureInfo.InvariantCulture)} |");

                for (int i = 0; i <= controlPoints.Count - 3; i++)
                {
                    double coefficient = FindBernsteinPolynomial(controlPoints.Count - 1, i, t);
                    row.Append($" {coefficient.ToString("F4", CultureInfo.InvariantCulture)} |");
                }

                table.AppendLine(row.ToString());
            }

            return table.ToString();
        }

        private List<Point> FindPoints(double step)
        {
            PointCollection points = bezierCurvePolyline.Points; // Отримання колекції точок полілінії

            List<Point> foundPoints = new List<Point>();

            for (double x = points.Min(p => p.X); x <= points.Max(p => p.X); x += step)
            {
                for (int i = 1; i < points.Count; i++)
                {
                    Point p1 = points[i - 1];
                    Point p2 = points[i];

                    // Перевіряємо, чи значення X лежить між X-координатами точок p1 і p2
                    if ((p1.X <= x && x <= p2.X) || (p2.X <= x && x <= p1.X))
                    {
                        // Інтерполяція для знаходження відповідної Y-координати на лінії між точками p1 і p2
                        double yValue = p1.Y + (x - p1.X) * (p2.Y - p1.Y) / (p2.X - p1.X);

                        foundPoints.Add(new Point(x, yValue));
                    }
                }
            }

            return foundPoints;
        }

        private string PrintBezierPoints(double step)
        {
            StringBuilder stringBuilder = new StringBuilder();
            List<Point> points = FindPoints(step * INTERVAL);
            stringBuilder.AppendLine($"Bezier points with step {step}:");

            HashSet<string> uniquePoints = new HashSet<string>();

            foreach (var point in points)
            {
                double scaledX = Math.Round((point.X - canvas.Width / 2) / INTERVAL, 3);
                double scaledY = Math.Round((-1) * (point.Y - canvas.Height / 2) / INTERVAL, 3);
                string formattedPoint = $"( {scaledX} ; {scaledY} )";

                // Додати унікальні точки до хешсету, щоб видалити дублікати
                if (!uniquePoints.Contains(formattedPoint))
                {
                    uniquePoints.Add(formattedPoint);
                    stringBuilder.AppendLine(formattedPoint);
                }
            }

            return stringBuilder.ToString();
        }
    }
}
