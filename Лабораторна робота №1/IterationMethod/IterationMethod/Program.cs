﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IterationMethod
{
    internal class Program
    {

        static double FindPartialDerivative(double dValue, char cXorY, int nFunctionNumber, double par)
        {

            if (cXorY == 'x')
            {

                if (nFunctionNumber == 1)
                    return -2 * (par - dValue);
                else if (nFunctionNumber == 2)
                    return -2 * (par - dValue);
                else
                    throw new ArgumentException("There is no such function!");

            }
            else if (cXorY == 'y')
            {

                if (nFunctionNumber == 1)
                    return -2 * (par - dValue);
                else if (nFunctionNumber == 2)
                    return -2 * (par - dValue);
                else
                    throw new ArgumentException("There is no such function!");
            }
            else
            {
                throw new ArgumentException("There is no such variable");
            }
        }

        static void FindSolutionNM(double dX, double dY, double dEpsilon, double[,] par)
        {

            double dPrevX = 0, dPrevY = 0, dDifference = 0, dDetJacobi = 0,
                    dDeltaX = 0, dDeltaY = 0;

            int nIterations = 0;

            double[,] aJacobian = new double[2, 2];
            double[,] aAddMatrix = new double[2, 2];
            double[] aFunctions = new double[2];

            do
            {
                dPrevX = dX;
                dPrevY = dY;

                aJacobian[0, 0] = FindPartialDerivative(dPrevX, 'x', 1, par[0, 0]);
                aJacobian[0, 1] = FindPartialDerivative(dPrevX, 'y', 1, par[0, 1]);
                aJacobian[1, 0] = FindPartialDerivative(dPrevX, 'x', 2, par[1, 0]);
                aJacobian[1, 1] = FindPartialDerivative(dPrevX, 'y', 2, par[1, 1]);

                dDetJacobi = FindDeterminant(aJacobian);

                if (dDetJacobi is 0)
                {
                    /*std::cout << "The system has no solutions!" << std::endl;*/
                    return;
                }

                for (int i = 0; i < 2; ++i)
                {

                    aAddMatrix[i, 0] = FindFunctionValue(dPrevX, dPrevY, i + 1, par);
                    //aAddMatrix[i][1] = FindPartialDerivative(dPrevX, 'y', i + 1);
                    aAddMatrix[i, 1] = aJacobian[i, 1];
                }

                dDeltaX = FindDeterminant(aAddMatrix) / dDetJacobi * (-1);


                for (int i = 0; i < 2; ++i)
                {

                    //aAddMatrix[i][0] = FindPartialDerivative(dPrevX, 'x', i + 1);
                    aAddMatrix[i, 0] = aJacobian[i, 0];
                    aAddMatrix[i, 1] = FindFunctionValue(dPrevX, dPrevY, i + 1, par);
                }

                dDeltaY = FindDeterminant(aAddMatrix) / dDetJacobi * (-1);

                dX = dPrevX + dDeltaX;
                dY = dPrevY + dDeltaY;
                /*std::cout << dX << "   " << dY << std::endl;*/

                ++nIterations;
            } while (Math.Abs(dDeltaX) > dEpsilon || Math.Abs(dDeltaY) > dEpsilon);

            /*std::cout << "The Solution:" << std::endl << std::endl;*/
            /*std::cout << dX << "   " << dY << std::endl << std::endl;*/
            /*std::cout << "The amount of iterations: " << nIterations << std::endl << std::endl;*/
            Console.WriteLine($"x: {dX}; y: {dY}");
        }

        static double FindFunctionValue(double dX, double dY, int nFunctionNumber, double[,] par)
        {

            if (nFunctionNumber == 1)
            {

                return Math.Pow((par[0, 0] - dX), 2) + Math.Pow((par[0, 1] - dY), 2) - 13;
            }
            else if (nFunctionNumber == 2)
            {

                return Math.Pow((par[1, 0] - dX), 2) + Math.Pow((par[1, 1] - dY), 2) - 13;
            }
            else
            {
                throw new ArgumentException("There is no such function!");
            }
        }

        static double FindDeterminant(double[,] dMatrix)
        {

            return dMatrix[0, 0] * dMatrix[1, 1] - dMatrix[0, 1] * dMatrix[1, 0];
        }
        static void Main(string[] args)
        {
            double[,] par = { { 1, 4 }, { 4, 2 } };
            FindSolutionNM(0, 0, 1, par);
        }
    }
}
