﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

//Клас, що представляє користувацький елемент управління "MenuBar" зі вкладеними опціями.
namespace TriangleCreator.View.UserControls
{
    /// <summary>
    /// Interaction logic for MenuBar.xaml
    /// </summary>
    /// 

    public enum ShapeType
    {
        None,
        Circle,
        Square
    }

    public partial class MenuBar : System.Windows.Controls.UserControl
    {

        private Color color = Colors.Transparent; // Закрите поле кольору за замовчуванням.

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        private ShapeType _selectedShape; // Закрите поле обраної форми.

        public ShapeType SelectedShape
        {
            get { return _selectedShape; }
            set
            {
                _selectedShape = value;
            }
        }

        public MenuBar()
        {
            InitializeComponent();
        }

        // Обробник зміни вибору в комбінованому списку.
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox.SelectedItem != null)
            {
                ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;
                string shape = selectedItem.Tag.ToString();

                if (Enum.TryParse(shape, out ShapeType result))
                {
                    SelectedShape = result;
                }
            }
        }

        // Метод для показу діалогового вікна вибору кольору.
        private void ShowColorPickerDialog(object sender, RoutedEventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();

            DialogResult result = colorDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                System.Drawing.Color color = colorDialog.Color;

                System.Windows.Media.Color wpfColor = System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B);

                this.color = wpfColor;

                fillColor.Background = new SolidColorBrush(wpfColor);
            }
        }
    }
}
