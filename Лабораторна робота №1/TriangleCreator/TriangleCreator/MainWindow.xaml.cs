﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriangleCreator.View.UserControls;

namespace TriangleCreator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool created = false; // Прапорець для визначення, чи були створені осі графіка.
        private double _maxX; // Максимальне значення по осі X.
        private double _maxY; // Максимальне значення по осі Y.
        private const int INTERVAL = 20;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void bGenerate_Clicked(object sender, EventArgs e)
        {
            // Перевірка чи створено осі
            if (created is false)
            {
                MessageBox.Show("You have not created axises!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            double x1, x2, y1, y2;
            bool[] check = new bool[6];
            check[0] = double.TryParse(X1.tboxCoordinate.Text, out x1);
            check[1] = double.TryParse(X2.tboxCoordinate.Text, out x2);
            check[2] = double.TryParse(Y1.tboxCoordinate.Text, out y1);
            check[3] = double.TryParse(Y2.tboxCoordinate.Text, out y2);
            check[4] = double.TryParse(maxX.tboxCoordinate.Text, out _maxX);
            check[5] = double.TryParse(maxY.tboxCoordinate.Text, out _maxY);


            // Перевірка на коректність введених координат
            if (Array.Exists(check, e => e == false))
            {
                MessageBox.Show("You have entered incorrect data!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double minX = _maxX * (-1), minY = (-1) * _maxY;
            if (x1 > _maxX || x1 < minX || x2 > _maxX || x2 < minX || y1 > _maxY || y1 < minY || y2 > _maxY || y2 < minY)
            {
                string error = $"Check your coordinates! They must follow such a rule: " +
                    $"{minX} <= x <= {_maxX}; {minY} <= y <= {_maxY}";

                MessageBox.Show(error, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Point point1 = new Point(cGraph.Width / 2 + x1 * INTERVAL, cGraph.Height / 2 - y1 * INTERVAL);
            Point point2 = new Point(cGraph.Width / 2 + x2 * INTERVAL, cGraph.Height / 2 - y2 * INTERVAL);

            if (point1 == point2)
            {
                MessageBox.Show("The two points cannot be the same!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Point point3 = CalculateThirdVertex(point1, point2);
            tbX3.Text = ((point3.X - cGraph.Width / 2) / INTERVAL).ToString("N1");
            tbY3.Text = ((cGraph.Height / 2 - point3.Y) / INTERVAL).ToString("N1");

            if (point3.X < 0 || point3.X > cGraph.Width || point3.Y < 0 || point3.Y > cGraph.Height)
            {
                MessageBox.Show("The calculated third point is out of bounds!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DrawTriangle(point1, point2, point3);
        }

        private void bCreate_Clicked(object sender, EventArgs e)
        {
            bool[] check = new bool[2];

            check[0] = double.TryParse(maxX.tboxCoordinate.Text, out _maxX);
            check[1] = double.TryParse(maxY.tboxCoordinate.Text, out _maxY);

            // Перевірка на коректність введених даних
            if (Array.Exists(check, e => e == false) || _maxX <= 0 || _maxY <= 0 || _maxX > 100 || _maxY > 100)
            {
                MessageBox.Show("You have entered incorrect data!", "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Обчислення та встановлення висоти та ширини полотна відносно заданих користувачем максимальних значень х та у
            cGraph.Width = (_maxX + 1) * 2 * INTERVAL;
            cGraph.Height = (_maxY + 1) * 2 * INTERVAL;
            cGraph.Children.Clear();

            // Створення осей
            CreateGraphAxis();

            // Створення підписів
            TextBlock text = new TextBlock();
            text.Text = "0";
            text.Margin = new Thickness((cGraph.Width / 2) + 2, cGraph.Height / 2 + 2, 0, 0);
            cGraph.Children.Add(text);

            TextBlock textX = new TextBlock();
            textX.Text = "X";
            textX.Margin = new Thickness(cGraph.Width - 10, cGraph.Height / 2 + INTERVAL, 0, 0);
            cGraph.Children.Add(textX);

            TextBlock textY = new TextBlock();
            textY.Text = "Y";
            textY.Margin = new Thickness((cGraph.Width / 2) + INTERVAL, 0, 0, 0);
            cGraph.Children.Add(textY);

            for (int i = INTERVAL, j = (-1) * i; i < (int)(cGraph.Width / 2) && j > (int)(cGraph.Width / 2 * (-1)); i += INTERVAL, j -= INTERVAL)
            {
                WriteHorAxis(i, 5);
                WriteHorAxis(j, -15);
            }

            for (int i = INTERVAL, j = (-1) * i; i < (int)(cGraph.Height / 2) && j > (int)(cGraph.Height / 2 * (-1)); i += INTERVAL, j -= INTERVAL)
            {
                WriteVerAxis(i, 0);
                WriteVerAxis(j, 0);
            }

            created = true;
        }

        private void CreateGraphAxis()
        {
            Line horAxis = new Line();
            Line verAxis = new Line();

            horAxis.X1 = 0;
            horAxis.X2 = cGraph.Width;
            horAxis.Y1 = horAxis.Y2 = cGraph.Height / 2;
            horAxis.Stroke = Brushes.Black;
            horAxis.StrokeThickness = 2;

            Line x_RightArrow = new Line();
            x_RightArrow.X1 = cGraph.Width;
            x_RightArrow.X2 = cGraph.Width - 10;
            x_RightArrow.Y1 = cGraph.Height / 2;
            x_RightArrow.Y2 = cGraph.Height / 2 - 10;
            x_RightArrow.Stroke = Brushes.Black;
            x_RightArrow.StrokeThickness = 2;
            cGraph.Children.Add(x_RightArrow);

            Line x_LeftArrow = new Line();
            x_LeftArrow.X1 = cGraph.Width;
            x_LeftArrow.X2 = cGraph.Width - 10;
            x_LeftArrow.Y1 = cGraph.Height / 2;
            x_LeftArrow.Y2 = cGraph.Height / 2 + 10;
            x_LeftArrow.Stroke = Brushes.Black;
            x_LeftArrow.StrokeThickness = 2;
            cGraph.Children.Add(x_LeftArrow);


            verAxis.X1 = verAxis.X2 = cGraph.Width / 2;
            verAxis.Y1 = 0;
            verAxis.Y2 = cGraph.Height;
            verAxis.Stroke = Brushes.Black;
            verAxis.StrokeThickness = 2;

            Line y_RightArrow = new Line();
            y_RightArrow.X1 = cGraph.Width / 2;
            y_RightArrow.X2 = cGraph.Width / 2 + 10;
            y_RightArrow.Y1 = 0;
            y_RightArrow.Y2 = 10;
            y_RightArrow.Stroke = Brushes.Black;
            y_RightArrow.StrokeThickness = 2;
            cGraph.Children.Add(y_RightArrow);

            Line y_LeftArrow = new Line();
            y_LeftArrow.X1 = cGraph.Width / 2;
            y_LeftArrow.X2 = cGraph.Width / 2 - 10;
            y_LeftArrow.Y1 = 0;
            y_LeftArrow.Y2 = 10;
            y_LeftArrow.Stroke = Brushes.Black;
            y_LeftArrow.StrokeThickness = 2;
            cGraph.Children.Add(y_LeftArrow);


            cGraph.Children.Add(horAxis);
            cGraph.Children.Add(verAxis);
        }

        private void WriteHorAxis(int position, int offset)
        {
            // Створення підписів, позначок та сітки (горизонтально)
            TextBlock textX = new TextBlock();
            textX.Text = (position / INTERVAL).ToString();
            textX.Margin = new Thickness((cGraph.Width / 2) + position + offset, cGraph.Height / 2 + 2, 0, 0);
            cGraph.Children.Add(textX);

            Line markX = new Line();
            markX.X1 = (cGraph.Width / 2) + position;
            markX.X2 = (cGraph.Width / 2) + position;
            markX.Y1 = 0;
            markX.Y2 = cGraph.Height;
            markX.Stroke = Brushes.Black;
            markX.StrokeThickness = 0.5;
            cGraph.Children.Add(markX);

            Line _markX = new Line();
            _markX.X1 = (cGraph.Width / 2) + position;
            _markX.X2 = (cGraph.Width / 2) + position;
            _markX.Y1 = (cGraph.Height / 2) - 5;
            _markX.Y2 = (cGraph.Height / 2) + 5;
            _markX.Stroke = Brushes.Black;
            _markX.StrokeThickness = 1.5;
            cGraph.Children.Add(_markX);
        }

        private void WriteVerAxis(int position, int offset)
        {
            // Створення підписів, позначок та сітки (вертикально)
            TextBlock textY = new TextBlock();
            textY.Text = (position / INTERVAL).ToString();
            textY.Margin = new Thickness((cGraph.Width / 2) + 5, cGraph.Height / 2 - position + offset, 0, 0);
            cGraph.Children.Add(textY);

            Line markY = new Line();
            markY.X1 = 0;
            markY.X2 = cGraph.Width;
            markY.Y1 = cGraph.Height / 2 + position;
            markY.Y2 = cGraph.Height / 2 + position;
            markY.Stroke = Brushes.Black;
            markY.StrokeThickness = 0.5;
            cGraph.Children.Add(markY);

            Line _markY = new Line();
            _markY.X1 = (cGraph.Width / 2) + 5;
            _markY.X2 = (cGraph.Width / 2) - 5;
            _markY.Y1 = cGraph.Height / 2 + position;
            _markY.Y2 = cGraph.Height / 2 + position;
            _markY.Stroke = Brushes.Black;
            _markY.StrokeThickness = 1.5;
            cGraph.Children.Add(_markY);
        }

        private Point CalculateThirdVertex(Point firstVertex, Point secondVertex)
        {
            // Обчислення 3 точки трикутника

            double thirdVertexX = (firstVertex.X + secondVertex.X) / 2 - (secondVertex.Y - firstVertex.Y) * Math.Sqrt(3) / 2;
            double thirdVertexY = (firstVertex.Y + secondVertex.Y) / 2 + (secondVertex.X - firstVertex.X) * Math.Sqrt(3) / 2;

            return new Point(thirdVertexX, thirdVertexY);
        }

        private void DrawTriangle(Point point1, Point point2, Point point3)
        {
            // Побудова трикутника на основі заданих точок, обраних вершин та кольору
            Polygon triangle = new Polygon();
            triangle.Stroke = Brushes.Black;
            triangle.StrokeThickness = 2;
            triangle.Fill = new SolidColorBrush(myMenuBar.Color);

            triangle.Points.Add(point1);
            triangle.Points.Add(point2);
            triangle.Points.Add(point3);

            cGraph.Children.Add(triangle);

            if (myMenuBar.SelectedShape == ShapeType.Circle)
            {
                DrawCirclePoint(point1);
                DrawCirclePoint(point2);
                DrawCirclePoint(point3);
            }
            else if (myMenuBar.SelectedShape == ShapeType.Square)
            {
                DrawSquarePoint(point1);
                DrawSquarePoint(point2);
                DrawSquarePoint(point3);
            }
        }

        private void DrawCirclePoint(Point point)
        {
            // Створення кілець - вершин трикутника з радіусом 2 пікселі
            Ellipse ellipse = new Ellipse();
            ellipse.Width = ellipse.Height = 10;
            if (myMenuBar.Color == Colors.Transparent)
                ellipse.Fill = Brushes.Black;
            else
                ellipse.Fill = new SolidColorBrush(myMenuBar.Color);
            ellipse.Margin = new Thickness(point.X - 5, point.Y - 5, 0, 0);
            cGraph.Children.Add(ellipse);
        }

        private void DrawSquarePoint(Point point)
        {
            // Створення квадратів - вершин трикутника з шириною та висотою 10 пікселів
            Rectangle square = new Rectangle();
            square.Width = square.Height = 10;
            if (myMenuBar.Color == Colors.Transparent)
                square.Fill = Brushes.Black;
            else
                square.Fill = new SolidColorBrush(myMenuBar.Color);
            square.Margin = new Thickness(point.X - 5, point.Y - 5, 0, 0);
            cGraph.Children.Add(square);
        }
    }
}
